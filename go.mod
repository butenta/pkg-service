module bitbucket.org/butenta/pkg-service

go 1.15

require (
	bitbucket.org/butenta/pkg-log v0.0.0-20200109054207-cee8abcd4519
	github.com/kardianos/service v1.2.0
)
